﻿/*
 * Created by SharpDevelop.
 * User: Dunkstormen
 * Date: 10-05-2017
 * Time: 10:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Password_Projekt
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		int LastFocus = 0;
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			if ( textBox1.Text == "abc" ) {
				
				groupBox1.Visible = false;
				groupBox2.Visible = true;
				
				
			} else {
				
				MessageBox.Show("Det indtastede password er forkert!");
				textBox1.Text = "";
			}
			
		}
		
		void TextBox2TextChanged(object sender, EventArgs e)
		{
			
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "7";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "7";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "8";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "8";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}
		}
		
		void Button4Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "9";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "9";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}
		}
		
		void Button5Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "4";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "4";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}
		}
		
		void Button6Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "5";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "5";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}
		}
		
		void Button7Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "6";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "6";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}			
		}
		
		void Button8Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "1";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "1";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}			
		}
		
		void Button9Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "2";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "2";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}			
		}
		
		void Button10Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "3";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "3";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}			
		}
		
		void Button11Click(object sender, EventArgs e)
		{
			if (LastFocus == 1) {
				textBox2.Text = textBox2.Text + "0";
			} else if (LastFocus == 2) {
				textBox3.Text = textBox3.Text + "0";
			} else {
				MessageBox.Show("Du har ikke valgt et felt!");
			}
			
		}
		
		void TextBox2Leave(object sender, EventArgs e)
		{
			LastFocus = 1;
		}
		
		void TextBox3Leave(object sender, EventArgs e)
		{
			LastFocus = 2;
		}
				
		// Regne funktioner
		void Button12Click(object sender, EventArgs e)
		{
			int a;
			int b;
			int res;
			
			a = int.Parse(textBox2.Text);
			b = int.Parse(textBox3.Text);
			
			res = a + b;
			
			textBox4.Text = res.ToString();
		}
		
		void Button13Click(object sender, EventArgs e)
		{
			int a;
			int b;
			int res;
			
			a = int.Parse(textBox2.Text);
			b = int.Parse(textBox3.Text);
			
			res = a - b;
			
			textBox4.Text = res.ToString();
		}
		
		void Button14Click(object sender, EventArgs e)
		{
			int a;
			int b;
			int res;
			
			a = int.Parse(textBox2.Text);
			b = int.Parse(textBox3.Text);
			
			res = a * b;
			
			textBox4.Text = res.ToString();
		}
		
		void Button15Click(object sender, EventArgs e)
		{
			int a;
			int b;
			int res;
			
			a = int.Parse(textBox2.Text);
			b = int.Parse(textBox3.Text);
			
			res = a / b;
			
			textBox4.Text = res.ToString();
		}
	}
}
